import sys

# Third party libraries path must be fixed before importing endpoints_proto_datastore
sys.path.insert(0, 'lib')

import endpoints
from protorpc import remote

from backend.models.dicom_file import DicomFile

package = 'DicomStore'

# prefix API calls with /_ah/api/ds/v0
@endpoints.api(name='ds', version='v0', description='Dicom Store API')
class DicomStoreApi(remote.Service):
    """Dicom Store API v0."""

    @DicomFile.query_method(query_fields=('limit', 'order', 'pageToken'),
                            path='dicom_files', name='dicom_files.list')
    def dicom_files_list(self, query):
        return query


application = endpoints.api_server([DicomStoreApi])

# dev server api explorer @ http://localhost:10080/_ah/api/explorer

