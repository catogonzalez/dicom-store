import sys
import os

# Third party libraries path must be fixed
sys.path.insert(0, 'frontend')
sys.path.insert(0, 'apps')
sys.path.insert(0, 'lib')

os.environ['DJANGO_SETTINGS_MODULE'] = 'frontend.settings'

# Force Django to reload its settings.
from django.conf import settings

settings._target = None

from django.core.wsgi import get_wsgi_application

frontend = get_wsgi_application()


# Backend webapp2 wsgi app declaration
import webapp2
import logging

routes = [
    (r'/_be/uploaded', 'backend.handlers.UploadHandler'),
]

config = {'webapp2_extras.sessions': {
    'secret_key': 'snd89n32u3xb333cr34c',
}}

backend = webapp2.WSGIApplication(routes=routes, debug=True, config=config)


def main():
    logging.getLogger().setLevel(logging.ALL)
    webapp2.util.run_wsgi_app(backend)


if __name__ == '__main__':
    main()
