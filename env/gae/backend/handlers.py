import json

from google.appengine.ext.webapp import blobstore_handlers
from google.appengine.ext import blobstore
from google.appengine.api import search

import dicom

from models.dicom_file import DicomFile


class UploadHandler(blobstore_handlers.BlobstoreUploadHandler):
    def post(self):
        # handles POST to '/_be/uploaded' when files have been successfully uploaded to GCS

        # read uploaded file(s)
        uploaded_files = self.get_uploads()
        for i in range(0, len(uploaded_files)):
            blob_info = uploaded_files[i]
            file_info = self.get_file_infos()[i]
            if blob_info and file_info:
                # save a new datastore instance of DicomFile model
                new_file = DicomFile()
                new_file.populate(
                    blob_key=blob_info.key(),
                    filename=file_info.filename,
                    content_type=file_info.content_type,
                    creation=file_info.creation,
                    size=file_info.size,
                    md5_hash=file_info.md5_hash,
                    gs_object_name=file_info.gs_object_name
                )

                tags = self.request.POST.get("tags-added", [])
                if len(tags) > 0:
                    new_file.tags = tags.split()

                # keep the new_file ndb key to use as an id in a google search Document
                new_file_key = new_file.put()

                # now save the DICOM Datasets to a google searchable Document
                blob_reader = blobstore.BlobReader(new_file.blob_key)
                ds = dicom.read_file(blob_reader)

                # create a fields collection to for the search Document and add standard fields
                fields = [search.TextField(name='filename', value=file_info.filename),
                          search.DateField(name='creation', value=file_info.creation),
                          search.NumberField(name='size', value=file_info.size),
                          search.TextField(name='tags', value=tags)]

                # now append DICOM DataElement values
                for element in ds:
                    try:
                        field_name = element.name.replace(' ', '_').lower()

                        # VR values based on http://oftankonyv.reak.bme.hu/tiki-index.php?page=Value+representation+in+DICOM
                        if element.VR in ('IS', 'DS', 'FL', 'FD', 'SL', 'SS', 'UL', 'US'):
                            fields.append(search.NumberField(name=field_name, value=element.value))

                        elif element.VR in ('DA', 'DT'):
                            fields.append(search.DateField(name=field_name, value=element.value))

                        else:
                            # default to TextField except if element name is 'Pixel Data' or VR is SQ
                            if element.name != 'Pixel Data' and element.VR != 'SQ':
                                fields.append(search.TextField(name=field_name, value=element.value))

                    except Exception, err:
                        # ignore this field
                        # TODO: implement correct handling of all field types/values
                        pass

                # create a new google search Document using the new_file ndb key as it's ID
                my_document = search.Document(doc_id=str(new_file_key.id()), fields=fields)

                # save the Document to our default dicom_store index
                index = search.Index(name="dicom_files")
                index.put(my_document)

        # Send response after all uploaded files have been saved
        self.request.status_int = 200
        self.request.status_message = 'Ok'
        self.response.headers['Content-Type'] = 'application/json'
        obj = {
            'status': 'Files have been uploaded',
        }
        self.response.write(json.dumps(obj))
