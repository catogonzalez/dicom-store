from google.appengine.api import search
from google.appengine.ext import ndb
from protorpc import messages

from endpoints_proto_datastore.ndb import EndpointsModel, EndpointsAliasProperty


class DicomFile(EndpointsModel):
    blob_key = ndb.BlobKeyProperty()
    tags = ndb.StringProperty(repeated=True)
    filename = ndb.StringProperty()
    content_type = ndb.StringProperty()
    creation = ndb.DateTimeProperty()
    size = ndb.IntegerProperty()
    md5_hash = ndb.StringProperty()
    gs_object_name = ndb.StringProperty()

    # the actual DICOM dataset is stored in a search Document with the same id as the
    # corresponding DicomFile instance id (see backend.handlers.UploadHandler


    _message_fields_schema = ('blob_key', 'tags', 'filename', 'content_type', 'creation', 'size', 'md5_hash', 'dicom_dataset')

    def dummy(self, value):
        # dummy setter so that endpoints_proto_datastore API methods allow the server
        # to receive dicom_dataset properties
        pass


    @EndpointsAliasProperty(property_type=messages.StringField, setter=dummy)
    def dicom_dataset(self):
        index = search.Index(name="dicom_files")

        # Fetch the document containing all DICOM Datasets by its doc_id
        doc = index.get(str(self.id))
        if doc:
            return str(doc.doc_id)
        else:
            return None
