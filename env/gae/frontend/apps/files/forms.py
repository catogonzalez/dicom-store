from django import forms


# not used...

class DicomFileDetails(forms.Form):
    blob_key = forms.CharField(label='Stored blob key')
    tags = forms.CharField()
    filename = forms.CharField()
    content_type = forms.CharField()
    creation = forms.DateTimeField()
    size = forms.IntegerField
    md5_hash = forms.CharField()
    gs_object_name = forms.CharField()

