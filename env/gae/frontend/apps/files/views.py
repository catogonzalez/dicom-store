import json

from django.template import RequestContext
from django.shortcuts import render_to_response, render

from google.appengine.ext import blobstore
from google.appengine.api import urlfetch
from google.appengine.api import search as google_search


def index(request):
    # get query string from request
    q = request.GET.get('q', '')

    # get search fields collection from index
    indexes = google_search.get_indexes(limit=1, start_index_name='dicom_files', fetch_schema=True)
    if indexes.results[0]:
        search_index = indexes.results[0]
    else:
        search_index = None

    try:
        my_query = search_index.search(q)
        total_results = my_query.number_found

        file_list = []
        for doc in my_query.results:
            field_dict = {}
            for field in doc.fields:
                field_dict[field.name] = str(field.value)

            file_list.append({'doc_id': doc.doc_id, 'fields': field_dict})

            data = {'file_list': file_list, 'total_results': total_results,
                    'search_fields': sorted(search_index.schema)}

    except google_search.Error, e:
        data = {'google_search_error': e.message}

    return render_to_response('index.html', data, context_instance=RequestContext(request))


def upload(request):
    if request.method == 'GET':
        # we prefix success_path with /_be/ to have the backend (webapp2) application handle the saving
        # of google blobstore id within the db. This is done by backend.handlers.UploadHandler
        upload_url = blobstore.create_upload_url(success_path='/_be/uploaded', gs_bucket_name='dicom-store.appspot.com')

    data = {'upload_url': upload_url}
    return render_to_response('upload.html', data, context_instance=RequestContext(request))


def api_call(request):
    urlfetch.set_default_fetch_deadline(15)

    # get the list of files with a call to the REST API
    api_target = request.build_absolute_uri('/_ah/api/ds/v0/dicom_files')
    api_target = api_target.replace('http://', 'https://')

    result = urlfetch.fetch(api_target, validate_certificate=False)
    if result.status_code == 200:
        context = {'json_response': result.content}
        return render(request, 'api.html', context)
