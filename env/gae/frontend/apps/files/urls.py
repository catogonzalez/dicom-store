from django.conf.urls import patterns, url
from django.http import HttpResponseRedirect

from . import views

urlpatterns = patterns('',
                       (r'^$', lambda r: HttpResponseRedirect('/files/index')),
                       url(r'^index/$', views.index),
                       url(r'^index/(?P<q>\.*$)', views.index),
                       url(r'^upload/$', views.upload),
                       url(r'^api/$', views.api_call),
)