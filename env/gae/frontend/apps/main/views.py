import json
from google.appengine.api import urlfetch
from django.shortcuts import render


def home(request):
    # get the list of files with a call to the REST API
    urlfetch.set_default_fetch_deadline(15)
    api_target = request.build_absolute_uri('/_ah/api/ds/v0/dicom_files')
    api_target = api_target.replace('http://', 'https://')

    result = urlfetch.fetch(api_target, validate_certificate=False)
    if result.status_code == 200:
        file_list = json.loads(result.content).get('items', [])
        # save the number of items
        files_in_store = len(file_list)
        context = {'files_in_store': files_in_store}
    else:
        context = {'backend_error': result.content}

    return render(request, 'home.html', context)
